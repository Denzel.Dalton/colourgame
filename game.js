var numSquare = 6; 
var colors = []; 
var pickedColor;
var firstGuess = true;
var won = false;   
var score; 
var squares = document.querySelectorAll(".square"); 
var colorDisplay = document.querySelector("#colordisplay"); 
var message = document.querySelector("#correctdisplay"); 
var h1 = document.querySelector("h1"); 
var newColors = document.querySelector("#new"); 
var modeButtons = document.querySelectorAll(".Dif"); 

init(); 
function init(){
    score = 0; 
    //Adding event listeners to the mode buttons. 
    for(i = 0; i < modeButtons.length; i++){
        modeButtons[i].addEventListener("click", function(){
            modeButtons[0].classList.remove("selected"); 
            modeButtons[1].classList.remove("selected"); 
            this.classList.add("selected"); 
            if(this.innerHTML === "Easy")
                numSquare = 3; 
            else
                numSquare = 6; 
            
            
            reset(); 
    
        });
    }    
    //Adding colors and event listeners to the squares. 
    for(var i = 0; i < squares.length; i++){
        squares[i].addEventListener("click", function(){
            if(this.style.backgroundColor === pickedColor){
                changeColors(pickedColor); 
                message.innerHTML = "You Won!";
                h1.style.backgroundColor = pickedColor;
                newColors.innerHTML = "Play Again?"   
                if(firstGuess === true && won === false){
                    score++; 
                }
                won = true;  
                
            }
            else{
                firstGuess = false; 
                won = false; 
                score = 0; 
                this.style.backgroundColor = "rgb(27, 21, 21)"; 
                message.innerHTML = "Try Again"; 
            }
            document.querySelector("#score").innerHTML = score;
        }); 
    }
    reset(); 
}


function reset(){
    //Resets the game. 
    message.innerHTML = ""; 
    firstGuess = true; 
    won = false; 
    colors = makeColors(numSquare); 
    pickedColor = chooseColor(); 
    changeColors.innerHTML = "New Colors"; 
    colorDisplay.innerHTML = pickedColor;
    for(var i = 0; i < squares.length; i++){
        if(colors[i]){
            squares[i].style.display = "block"; 
            squares[i].style.backgroundColor = colors[i];
        }
        else{
            squares[i].style.display = "none"; 
        }
    }
         
    h1.style.backgroundColor = "goldenrod";
}

newColors.addEventListener("click", function(){
    newColors.innerHTML = "New Colors"; 
    reset(); 
});

colorDisplay.innerHTML = pickedColor; 


function makeColors(len){
    var arr = []; 

    for(i = 0; i < len; i++){
        arr.push(randomColor()); 
    }

    return arr; 
}
function randomColor(){
    red = Math.floor(Math.random() * 256); 
    green = Math.floor(Math.random() * 256); 
    blue = Math.floor(Math.random() * 256); 
    return "rgb("+red+", "+green+", "+blue + ")";  
}
function chooseColor(){
    return colors[Math.floor(Math.random() * colors.length)]; 
}
function changeColors(color){
    for(i = 0; i < squares.length; i++){
        squares[i].style.backgroundColor = color; 
    }
}